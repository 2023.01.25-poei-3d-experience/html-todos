
function onDeleteButtonClicked(evt) {
    let button = evt.currentTarget;
    //    - removes previous sibling (previousSibling(), remove())
    button.previousElementSibling.remove();
    //    - removes previous sibling again
    button.previousElementSibling.remove();
    //    - removes itself (remove())
    button.remove();
}


// delete todo ////////////////////////////////////////////////////////////////
// retrieve delete buttons and store them in a variable (querySelectorAll(), with selector: '.todos button')
let deleteButtons = document.querySelectorAll('.todos button');
// for each button, add an event listener on click event which 
for (let button of deleteButtons) {
    button.addEventListener("click", onDeleteButtonClicked);
}

// add todo ///////////////////////////////////////////////////////////////////
// retrieve the add button and store it in a variable (querySelector())
const addButton = document.querySelector(".add > button");
// retrieve the add input and store it in a variable (querySelector())
const addInput = document.querySelector(".add > input");
// retrieve the container containing the todos (class .todos) and store it (querySelector() again)
const todosDiv = document.querySelector(".todos");
// add an event listener on the click on the add button. On click :
addButton.addEventListener("click", evt => {
    //    - retrieve the input element value (value property)
    const label = addInput.value;
    //    - create 3 elements: input, div with label and remove button ()
    let template = document.createElement("div");
    template.innerHTML = "<input class='form-check-input' type='checkbox'>" +
                      "<div>" + label + "</div>" +
                      "<button class='btn btn-danger'>delete</button>";
    //    - attach these elements to the DOM ()
    todosDiv.appendChild(template.querySelector("input"));
    todosDiv.appendChild(template.querySelector("div > div"));
    todosDiv.appendChild(template.querySelector("button"));
    //    - add event listeners
    todosDiv.querySelector("button:last-child").addEventListener("click", onDeleteButtonClicked);
    //    - clear add input
    addInput.value = "";
    addButton.disabled = true;
})

// disable add button when the input is empty /////////////////////////////////
// add an event listener for 'input' on addInput which :
addInput.addEventListener("input", () => {
    //     - if the input value is empty => add disabled on addButton
    if (addInput.value === "")
        addButton.disabled = true;
    //     - else => remove disabled on addButton
    else
        addButton.disabled = false;
})



// // 1. All JS (we create and configure the elements in JS, then we insert them in the DOM)
// let newInput = document.createElement("input");
// newInput.classList.add("form-check-input")
// newInput.setAttribute("type", "checkbox");
// let newDiv = document.createElement("div");
// newDiv.innerHTML = "TEST JS";
// let newButton = document.createElement("button");
// newButton.classList.add("btn", "btn-danger");
// newButton.innerHTML = "delete";
// document.querySelector(".todos").appendChild(newInput);
// document.querySelector(".todos").appendChild(newDiv);
// document.querySelector(".todos").appendChild(newButton);

// // 2. We store a template in HTML, we clone it in JS, customize it then extract the elements and insert them in the DOM
// let template = document.querySelector("#template").cloneNode(true);
// document.querySelector(".todos").appendChild(template.querySelector("input"));
// newDiv = template.querySelector("div > div");
// newDiv.innerHTML = "TEST CLONE";
// document.querySelector(".todos").appendChild(newDiv);
// document.querySelector(".todos").appendChild(template.querySelector("button"));

// // 3. We create a dummy container and write the elements as a string (in JS), then we can extract the elements and insert them in the DOM
// let dummyDiv = document.createElement("template");
// let label = "TEST DUMMY DIV";
// dummyDiv.innerHTML = "<input class='form-check-input' type='checkbox'>" +
//     "<div>" + label + "</div>" +
//     "<button class='btn btn-danger'>delete</button>";
// document.querySelector(".todos").appendChild(template.querySelector("input"));
// document.querySelector(".todos").appendChild(template.querySelector("div > div"));
// document.querySelector(".todos").appendChild(template.querySelector("button"));